(ns dev.figwheel
  (:require [figwheel.client :as fw :include-macros true]
            [kban.client :refer [render]]))


(enable-console-print!)

(fw/watch-and-reload
  :jsload-callback
  #(render (. js/document (getElementById "app"))))
