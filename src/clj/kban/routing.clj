(ns kban.routing
       (:require [kban.render :refer [dev-wrapper]]
                 [taoensso.sente :as sente] 
                 [taoensso.sente.server-adapters.http-kit :refer  (sente-web-server-adapter)] 
                 [clojure.edn :as edn]
                 [compojure.core :refer [GET POST defroutes]]
                 [compojure.route :refer [resources not-found]]))


;; Sente (WebSocket/Ajax) init

(let  [{:keys  [ch-recv send-fn ajax-post-fn ajax-get-or-ws-handshake-fn
                connected-uids]}
       (sente/make-channel-socket! sente-web-server-adapter  {})]
  (def ring-ajax-post                ajax-post-fn)
  (def ring-ajax-get-or-ws-handshake ajax-get-or-ws-handshake-fn)
  (def ch-chsk                       ch-recv)         ; ChannelSocket's receive channel
  (def chsk-send!                    send-fn)         ; ChannelSocket's send API fn
  (def connected-uids                connected-uids)) ; Watchable, read-only atom

(def not-empty?
  (comp not empty?))

(def temp-cards-db
  (-> (slurp "cards.db")
      (edn/read-string)
      (atom)))

(def not-nil?
  (comp not nil?))

(defn edn-response [data & [status]]
        {:status (or status 200)
         :headers {"Content-Type" "application/edn"}
         :body (pr-str data)})

(defroutes routes
  (GET "/" [] (dev-wrapper {:view :home
                            :bins @temp-cards-db
                            :session {:uid nil}}))
  (GET  "/chsk" req  (ring-ajax-get-or-ws-handshake req))
  (POST "/chsk" req  (ring-ajax-post                req))
  (resources "/")
  (not-found "<h1>Page Not Found"))
