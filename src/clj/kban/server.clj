(ns kban.server
  (:require [org.httpkit.server :refer [run-server]]
            [clojure.core.async :refer [put! <! >! go go-loop chan]]
            [kban.routing :refer [routes temp-cards-db ch-chsk chsk-send! connected-uids]])
  (:use [ring.middleware.edn]
        [ring.middleware.keyword-params]
        [ring.middleware.params]
        [ring.middleware.resource]
        [ring.middleware.file-info])
  (:gen-class))

(defn edn-response [data & [status]]
  {:status (or status 200)
   :headers {"Content-Type" "application/edn"}
   :body (pr-str data)})


(go-loop []
  (let [{:keys [?data] :as res} (<! ch-chsk)
        data (-> ?data
                 :data
                 :bins)]
    (when data
      (reset! temp-cards-db data)
      (spit "cards.db" @temp-cards-db)
      (doseq [uid (:any @connected-uids)]
        (chsk-send! uid [:state/server-update data]))))
  (recur))

(defn -main [& {:as args}]
  (run-server (-> routes
                  (wrap-resource "public")
                  (wrap-file-info)
                  (wrap-keyword-params)
                  (wrap-params))
              {:host "0.0.0.0"
               :port 8080}))
