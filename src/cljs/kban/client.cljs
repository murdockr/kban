(ns kban.client
  (:require [om.core :as om :include-macros true]
            [cljs.core.async :refer [<! >! put! chan]]
            [components.components :refer [main]]
            [kban.routing :refer [init-history]]
            [cljs.reader :as edn]
            [taoensso.sente  :as sente :refer  (cb-success?)]
            [goog.dom :as gdom])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

;; Sente (Web Socket/Ajax) init

(let  [{:keys  [chsk ch-recv send-fn state]}
       (sente/make-channel-socket! "/chsk" {:type :auto})]
  (def chsk       chsk)
  (def ch-chsk    ch-recv) ; ChannelSocket's receive channel
  (def chsk-send! send-fn) ; ChannelSocket's send API fn
  (def chsk-state state))   ; Watchable, read-only atom



(enable-console-print!)

(declare app-state)

(defn render [app-id]
  (om/root main
           app-state
           {:target app-id
            :shared (let [bins (defn bins []
                                 (om/ref-cursor (:bins (om/root-cursor app-state))))] 
                      {:session (defn session []
                                  (om/ref-cursor (:session (om/root-cursor app-state))))
                       :bins bins})}))

(defn ^:export init
  [app-id state-id]
  (if (nil? app-state)
    (->> state-id
         gdom/getElement
         .-textContent
         edn/read-string
         atom
         (set! app-state)))
  (->> app-id
       gdom/getElement
       (render))
  (init-history app-state)
  (add-watch app-state :state-change (fn [_ _ old-state new-state]
                                       (if (not= old-state new-state)
                                         (chsk-send! [:state/update {:data new-state}]))))
  (go-loop []
  (let [{:keys [event]} (<! ch-chsk)]
    (let [new-state (-> event
                        last
                        last)]
      (when new-state 
        (if (and (not= new-state [:first-open? true]) 
                 (not= new-state [:csrf-token nil]))
          (swap! app-state assoc :bins new-state)))))
  (recur)))
