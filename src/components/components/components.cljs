(ns components.components
  (:require [om.core :as om :include-macros true]
            [om.dom :refer [render-to-str]]
            [om-tools.core :refer-macros [defcomponent defcomponentmethod]]
            [om-tools.dom :as dom :include-macros true]
            [cljs.reader :as edn]
            [cljs-uuid-utils.core :refer [make-random-uuid uuid-string]]
            [goog.string :as gstr]
            [goog.dom :as gdom]))

;; Fix console print on Nashorn and browser
(if (exists? js/console)
  (enable-console-print!)
  (set-print-fn! js/print))

(defn name->idx
  [n]
  (case n
    "New" 0
    "To-Do" 1
    "Waiting" 2
    "Projects" 3
    "Brainstorm" 4))

(defn display-status
  [status user-id]
  (case status
    :in-progress (dom/span 
                   (dom/small {:class "card-edited"} user-id)
                   (dom/i {:class "fa fa-spinner fa-spin card-status"
                           :style {:color "blue"}}))
    :complete (dom/span 
                (dom/small {:class "card-edited"} user-id)
                (dom/i {:class "fa fa-check-circle card-status"
                        :style {:color "green"}}))
    :unfinished (dom/span 
                  (dom/small {:class "card-edited"} user-id)
                  (dom/li {:class "fa fa-exclamation-circle card-status"
                           :style {:color "red"}}))))

(def not-empty?
  (comp not empty?))

(defn get-bin
  [ref-bins bin]
  (let [this-bin (filter #(= (:title %) bin) ref-bins)]
    this-bin))

(defn remove-card
  [cards card]
  (into [] (filter #(not= (:uuid %) (:uuid card)) cards)))

(defcomponent card-status-button
  [{:keys [icon color card-data status]} owner]
  (render [_]
    (dom/span {:class (str "status-button" (:bin card-data) "-" (:title card-data))}
      (dom/i {:class icon
              :style {:color color}
              :onClick #(let [session (om/get-shared owner [:session])
                              bins (om/get-shared owner [:bins])
                              user (:uid (session))]
                          (prn (bins))
                          (om/transact! card-data (fn [_]
                                                    (prn card-data)
                                                    (assoc card-data :last-edited-by user
                                                           :status status))))}))))

(defcomponent card-expanded-view
  [{:keys [title bin status notes uuid added-by last-edited-by] :as data} owner]
  (render [_]
  (dom/div
    (dom/hr {:class "card-hr"})
    (dom/i {:class "fa fa-remove card-remove"
            :onClick #(let [sd (om/get-shared owner [:bins])
                            this-bin (first (get-bin (sd) bin))]
                        (om/set-state! owner :expanded? false)
                        (om/transact! this-bin :cards (fn [cards]
                                                        (remove-card cards data))))})
    (dom/small {:class "card-added-by"}
               "Added By: " added-by)
    (dom/br)
    (dom/p notes)
    (dom/select {:class (str title "-move-dest")}
                (map #(dom/option {:value %} %) ["New" "To-Do" "Waiting" "Projects" "Brainstorm"]))
    (dom/button {:type "submit"
                 :class "pure-button"
                 :onClick #(let [dest (.-value (gdom/getElementByClass (str title "-move-dest")))
                                 bins (om/get-shared owner [:bins])]
                             (om/update! (bins) (let [current-idx (name->idx (:bin data))
                                                      current-bin (nth (bins) current-idx)
                                                      this-bin {:title (:bin data) :cards (remove-card (:cards current-bin) data)}
                                                      dest-idx (name->idx dest)
                                                      dest-bin (nth (bins) dest-idx)
                                                      card (assoc data :bin dest)]
                                                  (assoc (bins) current-idx this-bin dest-idx {:title dest :cards (conj (:cards dest-bin) card)}))))}    
                "Move")
    (dom/br)
    (dom/br)
    (map #(om/build card-status-button % {:react-key (str bin "-" uuid "-" (:status %))}) [{:icon "fa fa-spinner fa-spin"
                                                                                            :color "blue"
                                                                                            :status :in-progress
                                                                                            :card-data data}
                                                                                           {:icon "fa fa-check-circle"
                                                                                            :color "green"
                                                                                            :status :complete
                                                                                            :card-data data}
                                                                                           {:icon "fa fa-exclamation-circle"
                                                                                            :color "red"
                                                                                            :status :unfinished
                                                                                            :card-data data}]))))

(defcomponent card 
  [{:keys [title bin status notes uuid added-by last-edited-by] :as data} owner]
  (init-state [_]
    {:expanded? false})
  (render-state [_ {:keys [expanded?] :as state}]
    (dom/div {:class "card"}
      (dom/i {:class "fa fa-bars card-expand"
              :onClick #(om/set-state! owner :expanded? (not expanded?))})
        (display-status status last-edited-by)
        (dom/h3 
          title)
        (when expanded?
         (om/build card-expanded-view data {:react-key (str bin "-" uuid "-expanded")})))))

(defcomponent bin
  [{:keys [title cards] :as data} owner]
  (init-state [_]
    {:adding-card? false})
  (render-state [_ {:keys [adding-card?]}]
    (dom/div {:class "pure-u-1 pure-u-sm-1-6 kanban-bin"} 
      (dom/h3 {:class "kanban-bin-name"}
        title)
      (dom/i {:class "fa fa-plus-circle bin-add-card"
              :onClick #(om/set-state! owner :adding-card? (not adding-card?))})
      (dom/br)
      (dom/br) 
      (dom/br) 
      (when adding-card?
        (dom/div
         (dom/form {:class "pure-form pure-form-stacked"} 
           (dom/fieldset
          (dom/input {:type "text"
                      :class (str "new-card-" title " add-card ")
                      :placeholder "add a new card"})
          (dom/textarea {:class (str "card-notes-" title)})
          (dom/button {:type "submit"
                       :class "pure-button pure-button-primary"
                       :onClick #(do
                                   (.preventDefault %)
                                   (om/transact! data :cards (fn [cards]
                                                               (let [new-card (.-value (gdom/getElementByClass (str "new-card-" title)))
                                                                     card-notes (.-value (gdom/getElementByClass (str "card-notes-" title)))
                                                                     session (om/get-shared owner [:session])
                                                                     user (:uid (session))]
                                                                 (om/set-state! owner :adding-card? false)
                                                                 (if (not-empty? new-card)
                                                                   (conj cards {:title new-card
                                                                                :notes card-notes 
                                                                                :status :unfinished
                                                                                :added-by user 
                                                                                :last-edited-by user 
                                                                                :bin title
                                                                                :uuid (uuid-string (make-random-uuid))})
                                                                   cards))))
                                   (set! (.-value (gdom/getElementByClass (str "new-card-" title))) ""))}
                      "Add New Card")))))
      (dom/div 
        (map #(om/build card % {:react-key (str "card-" (:uuid %))}) cards)))))

(defmulti body
  (fn [data owner]
    (:view data)))

(defcomponentmethod body :home
  [{:keys [bins session] :as data} owner]
  (init-state [_]
    {:logged-in? false})
  (render-state [_ {:keys [logged-in?]}]
    (dom/div {:class "pure-g"}
    (if logged-in?
      (dom/div {:class "pure-u-1"}
        (dom/p
          "Logged in as: " (:uid session))
        (map #(om/build bin % {:react-key (str "bin-" (:title %))}) bins))
      (dom/div {:id "login-form"
                :class "pure-u-1"}
        (dom/form {:class "pure-form"}
          (dom/fieldset
        (dom/input {:type "text"
                    :class "user-id"
                    :placeholder "username"}))
        (dom/button {:class "pure-button pure-button-primary"
                     :type "submit"
                     :onClick #(do
                                 (.preventDefault %)
                                 (om/transact! data :session (fn [session]
                                                               (let [username (.-value (gdom/getElementByClass "user-id"))]
                                                                 (assoc session :uid username))))
                                 (om/set-state! owner :logged-in? true))}
                    "Login")))))))

(defcomponent main
  [data owner]
  (render [_]
    (dom/div
      (om/build body data))))

(defn ^:export render-to-string
  "Server-Side Render Helper"
  [edn]
  (->> edn
       edn/read-string
       (om/build main)
       render-to-str))
